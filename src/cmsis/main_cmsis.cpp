#include "samd21/include/samd21g18au.h"
// #include "Arduino.h"

extern "C" {
  void set_PM_asm();
}

void set_LED() { //pin18 (relié à BUILT_IN) est pin PA17  

  PORT->Group[0].PINCFG[17].bit.PMUXEN = 1; // n registre [0..31]
  PORT->Group[0].PMUX[8].bit.PMUXO = 4; // n registre [0...15] impair PMUXO pair PMUXE
}

void set_TCC() { // 
  
  TCC2->CTRLA.bit.PRESCALER = 0; // Prescaler Clock = /DIV1
  TCC2->CTRLA.bit.PRESCSYNC = 0;
  TCC2->CTRLA.bit.CPTEN1 = 1;
  TCC2->WAVE.bit.WAVEGEN = 2; //NPWM Set à match (CC1) Clear à TOP (PER)
  TCC2->PER.bit.PER = 0xFF;
  TCC2->CC[1].bit.CC = 0xFFFF; // initial set-up : light 100%
}

void Dim_Led(int _dim, bool _sens) { // _dim value set "enlightenment" of Led
  
  TCC2->CTRLA.bit.ENABLE = 0;

  TCC2->CC[1].bit.CC = _dim;
  TCC2->CTRLA.bit.ENABLE = _sens;
}

uint16_t _w = (0x7f+1); // _w will be passed into blink or dim function

int main(void) {
  set_PM_asm();
  set_LED();
  set_TCC();

  Dim_Led(_w,true);

  return 0;
}

// void setup() {
//   set_PM_asm();
//   set_LED();
//   set_TCC();

//   Dim_Led(_w, true);
// }

// void loop() {
//   // Nothing needs to be done in loop()
// }