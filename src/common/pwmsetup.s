.include "include/regs_pm.s"
.include "include/regs_port.s"

.func set_PM
set_PM_asm: // Power management and connect clock to TCC2
    // Enable TCC2 bus clock this is good practice but optional
    ldr r0, =REG_PM_APBCMASK // address of the AP
    ldr r1, =PM_APBCMASK_TCC2
    ldr r4, [r0]
    orr r4, r1
    str r4, [r0]

    // Enable, (GCLK clock | GCLK0 as generator | bind TCC2 to clock)
    ldr r2, =REG_GCLK_CLKCTRL
    ldr r3, =(GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0 | GCLK_CLKCTRL_ID_TCC2_TC3)
    strh r3, [r2]
    bx lr
.endfunc

.func set_LED
set_LED_asm:
    
.endfunc
.global set_PM_asm
