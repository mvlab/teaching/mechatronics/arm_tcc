// Power management registers
.equ    REG_PM_APBCMASK,                (0x40000420)
.equ    PM_APBCMASK_TCC2_Pos,           10
.equ    PM_APBCMASK_TCC2,               (0x1 << PM_APBCMASK_TCC2_Pos)

// GCLK registers
.equ    REG_GCLK_CLKCTRL,               (0x40000C02)
.equ    GCLK_CLKCTRL_CLKEN_Pos,         14
.equ    GCLK_CLKCTRL_CLKEN,             (0x1 << GCLK_CLKCTRL_CLKEN_Pos)
.equ    GCLK_CLKCTRL_GEN_GCLK0_Val,     0x0
.equ    GCLK_CLKCTRL_GEN_Pos,           8
.equ    GCLK_CLKCTRL_GEN_GCLK0,         (GCLK_CLKCTRL_GEN_GCLK0_Val << GCLK_CLKCTRL_GEN_Pos)
.equ    GCLK_CLKCTRL_ID_TCC2_TC3_Val,   0x1B
.equ    GCLK_CLKCTRL_ID_Pos,            0
.equ    GCLK_CLKCTRL_ID_TCC2_TC3,       (GCLK_CLKCTRL_ID_TCC2_TC3_Val << GCLK_CLKCTRL_ID_Pos)

// PORT registers
.equ    REG_PORT_PINCFG0,               (0x41004440)
.equ    REG_PORT_PMUX0,                 (0x41004430)
.equ    PORT_PINCFG_PMUXEN_Pos,         0
.equ    PORT_PINCFG_PMUXEN,             (0x1 << PORT_PINCFG_PMUXEN_Pos)
